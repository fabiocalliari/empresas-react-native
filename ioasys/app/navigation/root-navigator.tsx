import {
  CardStyleInterpolators,
  HeaderStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {EnterpriseDetails, Home, Landing, Search} from '_screens/index';
import {
  HeaderBackButton,
  HeaderSearchButton,
  PressBox,
  Text,
} from '_atoms/index';
import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import React, {forwardRef, useMemo} from 'react';
import {StatusBar, StatusBarStyle, TextInput} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {AppState} from 'app/store/reducers';
import {EnterpriseInfo} from '_reducers/enterprises/types';
import Ioasys from '_assets/ioasys.svg';
import {Theme} from '_theme/index';
import {setSearchContext} from '_reducers/search/actions';
import {useTheme} from '@shopify/restyle';

interface RootNavigatorProps {}

export type RootStackParamsList = {
  Landing: undefined;
  Home: undefined;
  EnterpriseDetails: {
    enterpriseInfo: EnterpriseInfo;
  };
  Search: undefined;
};

const Stack = createStackNavigator<RootStackParamsList>();

const RootStack: React.FC<RootNavigatorProps> = () => {
  const userProfile = useSelector<AppState>((store) => store.auth.userProfile);

  return (
    <Stack.Navigator
      screenOptions={({navigation: {canGoBack, goBack}}) => ({
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
        headerStyle: {
          elevation: 0,
          shadowOffset: {
            height: 0,
            width: 0,
          },
        },
        headerLeft: () => canGoBack() && <HeaderBackButton onTap={goBack} />,
        headerTitle: ({children}) => (
          <Text variant="subheader">{children}</Text>
        ),
      })}>
      {!userProfile && (
        <Stack.Screen
          name="Landing"
          component={Landing}
          options={{
            headerShown: false,
          }}
        />
      )}
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen
        name="EnterpriseDetails"
        component={EnterpriseDetails}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
          headerStyleInterpolator: HeaderStyleInterpolators.forSlideUp,
          headerTitle: null,
          headerTransparent: true,
          title: null,
        }}
      />
    </Stack.Navigator>
  );
};

const RootNavigator = forwardRef<NavigationContainerRef, RootNavigatorProps>(
  ({}, ref) => {
    const theme = useTheme<Theme>();
    const barStyle: StatusBarStyle = useMemo(
      () => (theme.dark ? 'light-content' : 'dark-content'),
      [theme],
    );
    return (
      <>
        <StatusBar
          backgroundColor="rgba(0,0,0,0)"
          barStyle={barStyle}
          translucent
        />
        <NavigationContainer ref={ref} theme={theme}>
          <RootStack />
        </NavigationContainer>
      </>
    );
  },
);

export default RootNavigator;
