import {ColorValue} from 'react-native';

const sumChars = (str: string) => {
  let sum = 0;
  for (let i = 0; i < str.length; i++) {
    sum += str.charCodeAt(i);
  }
  return sum;
};

export const generateBackgroundColor = (
  name: string,
  defaultColors: ColorValue[],
): ColorValue => {
  const i = sumChars(name) % defaultColors.length;
  const background = defaultColors[i];
  return background;
};
