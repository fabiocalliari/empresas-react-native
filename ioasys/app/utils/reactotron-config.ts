import Reactotron from 'reactotron-react-native';
import {reactotronRedux} from 'reactotron-redux';
import reactotronSaga from 'reactotron-redux-saga';

const tron = Reactotron.configure()
  .useReactNative()
  .use(reactotronRedux())
  .use(reactotronSaga({except: ['']}))
  .connect();

tron.clear();

export default tron;
