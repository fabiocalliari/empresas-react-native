import tokens from '@shopify/polaris-tokens';

const pxToNumber = (px: string) => {
  return parseInt(px.replace('px', ''), 10);
};

export const spacing = {
  none: tokens.spacingNone,
  xxs: pxToNumber(tokens.spacingExtraTight),
  xs: pxToNumber(tokens.spacingTight),
  s: pxToNumber(tokens.spacingBaseTight),
  m: pxToNumber(tokens.spacingBase),
  l: pxToNumber(tokens.spacingLoose),
  xl: pxToNumber(tokens.spacingExtraLoose),
  xxl: 2 * pxToNumber(tokens.spacingExtraLoose),
};
