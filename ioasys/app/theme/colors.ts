import * as palette from './palette';

const defaultPalette = {
  colors: {
    /**
     * The main tinting color.
     */
    primary: palette.pink500,
    /**
     * The color of text over primary background.
     */
    onPrimary: palette.white,
    /**
     * The screen background.
     */
    background: palette.white,
    /**
     * The color used for cards.
     */
    card: palette.white,
    /**
     *  The default color of text in many components.
     */
    text: palette.black,
    /**
     * A subtle color used for borders and lines.
     */
    border: palette.grey400,
    /**
     * The color used for notifications.
     */
    notification: palette.red500,
    /**
     * The color of text over primary background.
     */
    onNotification: palette.white,
    /**
     * The color used for secondary information.
     */
    secondary: palette.grey700,
  },
  dark: false,
};

const darkPalette = {
  colors: {
    ...defaultPalette.colors,
    background: palette.black,
    card: palette.black,
    primary: palette.pink700,
    onPrimary: palette.pink200,
    text: palette.white,
    secondary: palette.grey200,
  },
  dark: true,
};

export {defaultPalette, darkPalette};
