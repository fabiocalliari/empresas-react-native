export const breakpoints = {
  phone: 0,
  tablet: 768,
  largeTablet: 1024,
};
