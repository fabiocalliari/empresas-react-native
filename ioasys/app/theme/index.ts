import {darkPalette, defaultPalette} from './colors';

import {borderRadii} from './border-radii';
import {breakpoints} from './breakpoints';
import {buttonVariants} from '_molecules/button';
import {createTheme} from '@shopify/restyle';
import {spacing} from './spacing';
import {textVariants} from '_atoms/text';
import {toastVariants} from '_molecules/toast';

const theme = createTheme({
  ...defaultPalette,
  borderRadii,
  breakpoints,
  buttonVariants,
  spacing,
  textVariants,
  toastVariants,
});

export const defaultTheme = theme;

export const darkTheme = {
  ...theme,
  ...darkPalette,
  colors: {
    ...theme.colors,
    ...darkPalette.colors,
  },
};

export type Theme = typeof theme;
