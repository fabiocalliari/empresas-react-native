const API_URL = 'https://empresas.ioasys.com.br';
const API_VERSION = 'v1';

/**
 * The options used to configure the API.
 */
export interface ApiConfig {
  /**
   * The URL of the api.
   */
  apiUrl: string;

  /**
   * The version of the api.
   */
  apiVersion: string;

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number;
}

/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG: ApiConfig = {
  apiUrl: API_URL,
  apiVersion: API_VERSION,
  timeout: 10000,
};
