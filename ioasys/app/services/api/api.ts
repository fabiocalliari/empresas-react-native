import * as Types from './';

import {ApiConfig, DEFAULT_API_CONFIG} from './api-config';
import axios, {AxiosInstance} from 'axios';

import {EnterpriseInfo} from '_reducers/enterprises/types';
import {UserProfile} from 'app/store/reducers/auth/types';

export default class Api {
  /**
   * The axios instance.
   */
  __client: AxiosInstance;

  /**
   * The instance config.
   */
  config: ApiConfig;

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config;

    this.__client = axios.create({
      baseURL: `${this.config.apiUrl}/api/${this.config.apiVersion}`,
      timeout: this.config.timeout,
      headers: {
        Accept: 'application/json',
      },
    });
  }

  /**
   * Generates an HTTP Request to get the credentials of the user.
   *
   * @param {object} credentials - user's identifications.
   * @param {string} credentials.email - user's email.
   * @param {string} credentials.password - user's password.
   * @returns {Promise<UserProfile>} userProfile - user profile,
   */
  signIn = async (
    credentials: Types.LoginCredentials,
  ): Promise<UserProfile> => {
    try {
      const response = await this.__client.post(
        '/users/auth/sign_in',
        credentials,
      );

      const convertUser = (raw: any) => {
        return {
          id: raw.id,
          investorName: raw.investor_name,
          email: raw.email,
          city: raw.city,
          country: raw.country,
          balance: raw.balance,
          photo: raw.photo,
          firstAccess: raw.first_access,
          superAngel: raw.super_angel,
        };
      };

      const resultUser = convertUser(response.data.investor);

      this.__client.defaults.headers['access-token'] =
        response.headers['access-token'];
      this.__client.defaults.headers.client = response.headers.client;
      this.__client.defaults.headers.uid = response.headers.uid;

      return resultUser;
    } catch (error) {
      const {response} = error;
      if (!response.data?.success) {
        throw response.data.errors.shift();
      }
      throw 'Erro interno, tente novamente dentro de alguns instantes';
    }
  };

  /**
   * Generates an HTTP Request to get a collection of enterprises.
   *
   * @returns {Promise<EnterpriseInfo[]>} enterpriseInfo - enterprise information,
   */
  getEnterprises = async (params?: {
    enterpriseTypes: string;
    name: string;
  }): Promise<EnterpriseInfo[]> => {
    try {
      const response = await this.__client.get('/enterprises', {
        params: {
          enterprise_types: params?.enterpriseTypes,
          name: params?.name,
        },
      });

      const convertEnterprise = (raw: any) => {
        return {
          id: raw.id,
          emailEnterprise: raw.email_enterprise,
          facebook: raw.facebook,
          twitter: raw.twitter,
          linkedIn: raw.linkedin,
          phone: raw.phone,
          ownEnterprise: raw.own_enterprise,
          enterpriseName: raw.enterprise_name,
          photo: `${this.config.apiUrl}${raw.photo}`,
          description: raw.description,
          city: raw.city,
          country: raw.country,
          value: raw.value,
          sharePrice: raw.share_price,
          enterpriseType: {
            id: raw.enterprise_type.id,
            enterpriseTypeName: raw.enterprise_type.enterprise_type_name,
          },
        };
      };

      const resultEnterprises = response.data.enterprises.map(
        convertEnterprise,
      );

      return resultEnterprises;
    } catch (error) {
      const {response} = error;
      if (!response.data?.success) {
        throw response.data.errors.shift();
      }
      throw 'Erro interno, tente novamente dentro de alguns instantes';
    }
  };
}
