import {ActivityIndicator, FlatList, Image, TextInput} from 'react-native';
import {
  Box,
  Chip,
  Divider,
  HeaderSearchButton,
  PressBox,
  Text,
} from '_atoms/index';
import React, {useCallback, useEffect, useState} from 'react';
import {
  selectEnterpriseTypes,
  selectEnterprises,
} from '_reducers/enterprises/selectors';
import {useDispatch, useSelector} from 'react-redux';

import {AppState} from '_reducers/index';
import {EnterpriseInfo} from '_reducers/enterprises/types';
import {GET_ENTERPRISES_REQUEST} from '_reducers/enterprises/actionTypes';
import {RootStackParamsList} from '_navigation/root-navigator';
import {StackNavigationProp} from '@react-navigation/stack';
import {Theme} from '_theme/index';
import {checkIfLoading} from '_reducers/ui/selectors';
import {getEnterprisesRequest} from '_reducers/enterprises/actions';
import {makeStyles} from '_theme/make-styles';
import {setSearchContext} from '_reducers/search/actions';
import {useDebounce} from '_hooks/index';
import {useFocusEffect} from '@react-navigation/native';
import {useTheme} from '@shopify/restyle';

type HomeNavigationProp = StackNavigationProp<RootStackParamsList, 'Home'>;
interface HomeProps {
  navigation: HomeNavigationProp;
}

const useStyles = makeStyles((theme) => ({
  horizontalList: {
    flexGrow: 0,
  },
  enterpriseListContentContainer: {
    flexGrow: 1,
    padding: theme.spacing.s,
  },
  image: {
    height: 120,
    width: 120,
  },
  input: {
    color: theme.colors.text,
  },
}));

const Home: React.FC<HomeProps> = ({navigation}) => {
  const styles = useStyles();
  const {colors} = useTheme<Theme>();

  const [type, setType] = useState(null);
  const [searchTerm, setSearchTerm] = useState<string>(null);
  const debouncedSearchTerm = useDebounce(searchTerm, 500);

  const searchContext = useSelector<AppState>(
    (store) => store.search.searchContext,
  );

  const enterprises = useSelector((state: AppState) =>
    selectEnterprises(state),
  );
  const enterpriseTypes = useSelector((state: AppState) =>
    selectEnterpriseTypes(state),
  );
  const isLoading = useSelector((state: AppState) =>
    checkIfLoading(state, GET_ENTERPRISES_REQUEST),
  );
  const isSearching = searchContext === 'searching';

  const dispatch = useDispatch();

  const keyExtractor = (item: EnterpriseInfo) => `${item.id}`;

  const renderItem = ({item}: {item: EnterpriseInfo; index: number}) => {
    const {navigate} = navigation;
    const {
      city,
      country,
      enterpriseName,
      enterpriseType,
      photo,
      sharePrice,
    } = item;
    return (
      <PressBox
        onTap={() => navigate('EnterpriseDetails', {enterpriseInfo: item})}>
        <Box alignItems="flex-start" flexDirection="row">
          <Image source={{uri: photo}} style={styles.image} />
          <Box flex={1} paddingHorizontal="s" justifyContent="space-between">
            <Box flex={1}>
              <Text numberOfLines={1} variant="subheader">
                {enterpriseName}
              </Text>
              <Box flexDirection="row">
                <Text
                  numberOfLines={1}
                  variant="body">{`${city} / ${country}`}</Text>
              </Box>
            </Box>
            <Box
              alignItems="center"
              flexDirection="row"
              justifyContent="space-between">
              <Chip text={enterpriseType.enterpriseTypeName} />
              <Text variant="subtitle">{`U$ ${sharePrice}`}</Text>
            </Box>
          </Box>
        </Box>
      </PressBox>
    );
  };

  const onScreenFocus = useCallback(() => {
    const {setOptions} = navigation;

    if (isSearching) {
      setOptions({
        headerLeft: () => {
          return <HeaderSearchButton />;
        },
        headerTitle: () => (
          <TextInput
            autoFocus
            onChangeText={(text) => setSearchTerm(text)}
            placeholder="Search for"
            placeholderTextColor={colors.text}
            value={searchTerm}
            style={styles.input}
          />
        ),
        headerRight: () => {
          return (
            <PressBox onTap={() => dispatch(setSearchContext('static'))}>
              <Text color="primary" variant="button" paddingHorizontal="s">
                Done
              </Text>
            </PressBox>
          );
        },
      });
    } else {
      setOptions({
        headerLeft: undefined,
        headerTitle: undefined,
        headerRight: () => {
          return (
            <HeaderSearchButton
              onTap={() => {
                dispatch(setSearchContext('searching'));
              }}
            />
          );
        },
      });
    }
  }, [colors, dispatch, isSearching, navigation, searchTerm, styles.input]);

  const getEnterprises = useCallback(
    (enterpriseTypes?: string, name?: string) => {
      const params = {} as any;

      if (enterpriseTypes) params.enterpriseTypes = enterpriseTypes;
      if (name) params.name = name;

      dispatch(getEnterprisesRequest(params));
    },
    [dispatch],
  );

  useEffect(() => {
    getEnterprises(type?.id, debouncedSearchTerm);
  }, [debouncedSearchTerm, getEnterprises, type]);

  useEffect(() => {
    if (searchContext === 'static') {
      setSearchTerm(null);
    }
  }, [searchContext]);

  useFocusEffect(onScreenFocus);

  if (isLoading) {
    return (
      <Box flex={1} alignItems="center" justifyContent="center">
        <ActivityIndicator color={colors.notification} size={48} />
      </Box>
    );
  }

  return (
    <>
      <FlatList
        data={enterpriseTypes}
        horizontal
        keyExtractor={(enterpriseType) => `${enterpriseType.id}`}
        renderItem={({item}) => {
          const isSelected = type?.id === item.id;
          return (
            <PressBox onTap={() => setType(isSelected ? null : item)}>
              <Box paddingHorizontal="xxs" paddingVertical="s">
                <Chip text={item.enterpriseTypeName} isSelected={isSelected} />
              </Box>
            </PressBox>
          );
        }}
        showsHorizontalScrollIndicator={false}
        style={styles.horizontalList}
      />
      <FlatList
        contentContainerStyle={styles.enterpriseListContentContainer}
        data={enterprises}
        ItemSeparatorComponent={() => <Divider />}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </>
  );
};

export default Home;
