import React, {useCallback, useRef} from 'react';

import {Box} from '_atoms/index';
import {TextInput} from 'react-native-gesture-handler';
import {useFocusEffect} from '@react-navigation/native';

const Search = ({navigation}) => {
  const searchInputRef = useRef<TextInput>(null);
  useFocusEffect(
    useCallback(() => {
      const {setOptions} = navigation;
      setOptions({
        headerTitle: () => (
          <TextInput
            autoFocus
            placeholder="Search for"
            placeholderTextColor="white"
            ref={(ref) => (searchInputRef.current = ref)}
          />
        ),
      });
    }, [navigation]),
  );
  return <Box />;
};

export default Search;
