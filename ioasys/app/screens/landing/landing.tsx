import {Box, SafeAreaBox, Text} from '_atoms/index';
import {
  Button,
  ControlledInput,
  ControlledPasswordInput,
} from '_components/molecules';
import {FormProvider, useForm} from 'react-hook-form';
import React, {useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Ioasys from '_assets/ioasys.svg';
import {TextInput as RNTextInput} from 'react-native';
import {SIGN_IN_REQUEST} from '_reducers/auth/actionTypes';
import {Theme} from '_theme/index';
import Validators from './validators';
import {checkIfLoading} from '_reducers/ui/selectors';
import {signInRequest} from '_reducers/auth/actions';
import {useTheme} from '@shopify/restyle';

interface SignInFormData {
  email: string;
  password: string;
}

interface LandingProps {}

const Landing: React.FC<LandingProps> = () => {
  //#region local variables
  const {colors} = useTheme<Theme>();
  const emailInputRef = useRef<RNTextInput>(null);
  const passwordInputRef = useRef<RNTextInput>(null);
  const formMethods = useForm();

  const dispatch = useDispatch();

  const isLoading = useSelector((state: AppState) =>
    checkIfLoading(state, SIGN_IN_REQUEST),
  );
  //#endregion

  //#region local functions
  const onSubmit = ({email, password}: SignInFormData) => {
    dispatch(signInRequest({email, password}));
  };
  //#endregion

  return (
    <>
      <SafeAreaBox backgroundColor="background" flex={1}>
        <Box alignItems="center" flex={1} justifyContent="center">
          <Ioasys fill={colors.text} />
        </Box>
        <Box
          backgroundColor="background"
          borderTopLeftRadius="m"
          borderTopRightRadius="m"
          elevation={24}
          paddingHorizontal="xl"
          shadowColor="primary"
          shadowOffset={{
            width: 0,
            height: 12,
          }}
          shadowOpacity={0.58}
          shadowRadius={16.0}>
          <Text
            marginHorizontal="m"
            marginVertical="l"
            textAlign="center"
            variant="body">
            Type your credentials to sign in
          </Text>
          <FormProvider {...formMethods}>
            <ControlledInput
              blurOnSubmit={false}
              label="E-mail"
              name="email"
              nextInput={passwordInputRef}
              placeholder="Type your e-mail"
              ref={emailInputRef}
              rules={Validators.email}
            />
            <ControlledPasswordInput
              label="Password"
              name="password"
              placeholder="Type your password"
              ref={passwordInputRef}
              rules={Validators.password}
            />
          </FormProvider>
          <Button
            isLoading={isLoading}
            marginVertical="l"
            onTap={!isLoading ? formMethods.handleSubmit(onSubmit) : undefined}
            variant="primary">
            Login
          </Button>
        </Box>
      </SafeAreaBox>
    </>
  );
};

export default Landing;
