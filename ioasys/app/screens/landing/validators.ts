export default {
  email: {
    required: 'O campo E-mail é obrigatório',
  },
  password: {
    required: 'O campo Senha é obrigatório',
  },
};
