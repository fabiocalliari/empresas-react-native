import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetScrollView,
} from '@gorhom/bottom-sheet';
import {Box, Divider, SheetBackground} from '_atoms/index';
import {Dimensions, Image, StyleSheet} from 'react-native';
import React, {useMemo} from 'react';

import AboutSection from './about-section';
import HeaderSection from './header-section';
import InfoSection from './info-section';
import {RootStackParamsList} from '_navigation/root-navigator';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Theme} from '_theme/index';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useTheme} from '@shopify/restyle';

type EnterpriseDetailsNavigationProp = StackNavigationProp<
  RootStackParamsList,
  'EnterpriseDetails'
>;
type EnterpriseDetailsRouteProp = RouteProp<
  RootStackParamsList,
  'EnterpriseDetails'
>;

interface EnterpriseDetailsProps {
  navigation: EnterpriseDetailsNavigationProp;
  route: EnterpriseDetailsRouteProp;
}

const screenHeight = Dimensions.get('screen').height;
const imageHeight = screenHeight / 3;

const EnterpriseDetails: React.FC<EnterpriseDetailsProps> = ({route}) => {
  const {params} = route;
  const {enterpriseInfo} = params;

  const {borderRadii} = useTheme<Theme>();
  const insets = useSafeAreaInsets();

  const snapPoints = useMemo(
    () => [screenHeight - insets.top - imageHeight + borderRadii.xl, '100%'],
    [borderRadii, insets],
  );

  return (
    <Box flex={1}>
      <Box height={imageHeight} width="100%">
        <Image
          source={{uri: enterpriseInfo.photo}}
          style={StyleSheet.absoluteFill}
        />
      </Box>
      <BottomSheet
        animateOnMount
        backdropComponent={BottomSheetBackdrop}
        backgroundComponent={SheetBackground}
        snapPoints={snapPoints}
        topInset={insets.top}>
        <BottomSheetScrollView>
          <HeaderSection
            city={enterpriseInfo.city}
            country={enterpriseInfo.country}
            enterpriseName={enterpriseInfo.enterpriseName}
            typeName={enterpriseInfo.enterpriseType.enterpriseTypeName}
          />
          <InfoSection
            sharePrice={enterpriseInfo.sharePrice}
            value={enterpriseInfo.value}
          />
          <AboutSection description={enterpriseInfo.description} />
          <Divider />
        </BottomSheetScrollView>
      </BottomSheet>
    </Box>
  );
};

export default EnterpriseDetails;
