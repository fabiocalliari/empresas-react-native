import {Box, Text} from '_atoms/index';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import chroma from 'chroma-js';
import {makeStyles} from '_theme/make-styles';

interface InfoSectionProps {
  sharePrice: string;
  value: string;
}

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: chroma(theme.colors.text).alpha(0.05).hex(),
  },
  roundedAvatar: {
    borderRadius: 99,
    padding: theme.spacing.s,
  },
}));

const InfoSection: React.FC<InfoSectionProps> = ({sharePrice, value}) => {
  const styles = useStyles();
  return (
    <Box flexDirection="row" paddingVertical="l" style={styles.container}>
      <Box
        flex={1}
        flexDirection="row"
        alignItems="center"
        justifyContent="center">
        <Icon
          color={chroma('#FC4B73').brighten(2).hex()}
          name="currency-usd"
          size={24}
          style={[styles.roundedAvatar, {backgroundColor: '#FC4B73'}]}
        />
        <Box marginHorizontal="s">
          <Text variant="subtitle">{sharePrice}</Text>
          <Text color="secondary" variant="caption">
            Share Price
          </Text>
        </Box>
      </Box>
      <Box
        flex={1}
        flexDirection="row"
        alignItems="center"
        justifyContent="center">
        <Icon
          color={chroma('#FEDF56').brighten(2).hex()}
          name="domain"
          size={24}
          style={[styles.roundedAvatar, {backgroundColor: '#FEDF56'}]}
        />
        <Box marginHorizontal="s">
          <Text variant="subtitle">{value}</Text>
          <Text color="secondary" variant="caption">
            Value
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

export default InfoSection;
