import {Box, PressBox, Text} from '_atoms/index';
import React, {useCallback, useMemo, useState} from 'react';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface AboutSectionProps {
  description: string;
}

const AboutSection: React.FC<AboutSectionProps> = ({description}) => {
  const [collapse, setCollapse] = useState<boolean>(true);
  const numberOfLines = useMemo(() => (collapse ? 3 : undefined), [collapse]);

  const toggleCollapse = useCallback(() => setCollapse((v) => !v), []);

  return (
    <Box paddingHorizontal="m" marginTop="m">
      <Text color="secondary" variant="subtitle">
        About the enterprise
      </Text>
      <Text variant="subheader">Description</Text>
      <Text marginTop="m" numberOfLines={numberOfLines} variant="body">
        {description}
      </Text>
      <PressBox onTap={toggleCollapse}>
        <Text marginTop="m" variant="overline">
          {collapse ? `Read more` : `Read less`}
          <Icon name={collapse ? 'chevron-down' : 'chevron-up'} />
        </Text>
      </PressBox>
    </Box>
  );
};

export default AboutSection;
