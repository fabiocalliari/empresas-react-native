import {Box, Chip, Text} from '_atoms/index';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import {type} from 'ramda';

interface HeaderSectionProps {
  city: string;
  country: string;
  enterpriseName: string;
  typeName: string;
}

const HeaderSection: React.FC<HeaderSectionProps> = ({
  city,
  country,
  enterpriseName,
  typeName,
}) => {
  return (
    <Box paddingHorizontal="m" marginBottom="m">
      <Text fontWeight="bold" numberOfLines={1} variant="header">
        {enterpriseName}
      </Text>
      <Text
        marginBottom="xs"
        numberOfLines={1}
        textTransform="uppercase"
        variant="caption">
        <Icon name="map-marker" />
        {city} {country}
      </Text>
      <Box alignItems="flex-start">
        <Chip text={typeName} />
      </Box>
    </Box>
  );
};

export default HeaderSection;
