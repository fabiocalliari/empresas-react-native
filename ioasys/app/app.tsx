import React, {useEffect, useMemo, useRef} from 'react';
import {
  SafeAreaProvider,
  initialWindowMetrics,
} from 'react-native-safe-area-context';
import {darkTheme, defaultTheme} from './theme';

import {Api} from '_services/api';
import BootSplash from 'react-native-bootsplash';
import {BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import {NavigationContainerRef} from '@react-navigation/native';
import {Provider} from 'react-redux';
import RootNavigator from '_navigation/root-navigator';
import {ThemeProvider} from '@shopify/restyle';
import Toaster from './core/toaster';
import configureStore from './store/configure-store';
import {useColorScheme} from 'react-native';

if (__DEV__) {
  import('./utils/reactotron-config').then(() =>
    console.log('Reactotron Configured'),
  );
}

const store = configureStore({preloadedState: {} as any, api: new Api()});

const App = () => {
  const navigationRef = useRef<NavigationContainerRef>(null);
  const colorScheme = useColorScheme();

  const theme = useMemo(
    () => (colorScheme === 'dark' ? darkTheme : defaultTheme),
    [colorScheme],
  );

  useEffect(() => {
    (async () => {
      await BootSplash.hide();
    })();
  }, []);

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <SafeAreaProvider initialMetrics={initialWindowMetrics}>
          <BottomSheetModalProvider>
            <RootNavigator ref={navigationRef} />
          </BottomSheetModalProvider>
          <Toaster />
        </SafeAreaProvider>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
