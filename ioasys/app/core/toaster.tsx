import {EdgeInsets, useSafeAreaInsets} from 'react-native-safe-area-context';
import {removeNotification, updateNotification} from '_reducers/ui/actions';
import {useDispatch, useSelector} from 'react-redux';

import {AppState} from '_store/reducers';
import R from 'ramda';
import React from 'react';
import {Toast} from '_molecules/index';

const Toaster = () => {
  const insets = useSafeAreaInsets();

  const edgeInsets = Object.keys(insets).reduce((accumulator, currentValue) => {
    return {
      ...accumulator,
      [currentValue]: insets[currentValue] + 8,
    };
  }, {}) as EdgeInsets;

  const dispatch = useDispatch();
  const {notifications} = useSelector((state: AppState) => state.ui);
  const notification = notifications?.[0];

  if (!notification) {
    return null;
  }

  const onAnimationFinish = () => {
    dispatch(removeNotification(notification.id));
  };

  const onClose = () => {
    dispatch(
      updateNotification({
        ...notification,
        open: false,
      }),
    );
  };

  return (
    <Toast
      autoHideDuration={3000}
      edgeInsets={edgeInsets}
      message={notification.message}
      open={notification.open}
      onAnimationFinish={onAnimationFinish}
      onClose={onClose}
      variant={notification.variant as any}
    />
  );
};

export default Toaster;
