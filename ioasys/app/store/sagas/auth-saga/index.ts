import {
  addNotification,
  startAction,
  stopAction,
} from '_store/reducers/ui/actions';
import {call, getContext, put, takeEvery} from 'redux-saga/effects';
import {signInFulfilled, signInRejected} from '_store/reducers/auth/actions';

import {Api} from 'app/services/api';
import {SIGN_IN_REQUEST} from '_store/reducers/auth/actionTypes';
import {SignInRequestAction} from '_store/reducers/auth/types';

function* signInRequest({type, payload}: SignInRequestAction) {
  try {
    yield put(startAction(type, payload));

    const api: Api = yield getContext('api');

    const response = yield call(api.signIn, payload.email, payload.password);

    yield put(signInFulfilled(response));
  } catch (e) {
    yield put(addNotification({message: e, variant: 'error'}));
    yield put(signInRejected(e));
  } finally {
    yield put(stopAction(type));
  }
}

export const sagas = {
  watchSignInRequest: takeEvery(SIGN_IN_REQUEST, signInRequest),
};
