import {all} from 'redux-saga/effects';
import {sagas as authSagas} from './auth-saga';
import {sagas as enterprisesSagas} from './enterprises-saga';

export default function* rootSaga() {
  yield all({
    ...authSagas,
    ...enterprisesSagas,
  });
}
