import {
  addNotification,
  startAction,
  stopAction,
} from '_store/reducers/ui/actions';
import {call, getContext, put, takeEvery} from 'redux-saga/effects';
import {
  getEnterprisesFulfilled,
  getEnterprisesRejected,
} from '_reducers/enterprises/actions';

import {Api} from 'app/services/api';
import {GET_ENTERPRISES_REQUEST} from '_reducers/enterprises/actionTypes';
import {GetEnterprisesRequestAction} from '_reducers/enterprises/types';

function* getEnterprisesRequest({type, payload}: GetEnterprisesRequestAction) {
  try {
    yield put(startAction(type, null));

    const api: Api = yield getContext('api');

    const response = yield call(api.getEnterprises, payload.params);

    yield put(getEnterprisesFulfilled(response));
  } catch (e) {
    yield put(addNotification({message: e, variant: 'error'}));
    yield put(getEnterprisesRejected(e));
  } finally {
    yield put(stopAction(type));
  }
}

export const sagas = {
  watchGetEnterprisesRequest: takeEvery(
    GET_ENTERPRISES_REQUEST,
    getEnterprisesRequest,
  ),
};
