import {EnterprisesActionTypes, EnterprisesState} from './types';
import {
  GET_ENTERPRISES_FULFILLED,
  GET_ENTERPRISES_REJECTED,
  GET_ENTERPRISES_REQUEST,
} from './actionTypes';
import produce, {enableES5} from 'immer';

enableES5();

const initialState: EnterprisesState = {
  enterprises: [],
  error: null,
};

export default (state = initialState, action: EnterprisesActionTypes) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_ENTERPRISES_REQUEST: {
        draft.enterprises = [];
        draft.error = null;
        break;
      }
      case GET_ENTERPRISES_FULFILLED: {
        draft.enterprises = action.payload.enterprises;
        break;
      }
      case GET_ENTERPRISES_REJECTED: {
        draft.enterprises = [];
        draft.error = action.payload.error;
        break;
      }
      default:
        return draft;
    }
  });
