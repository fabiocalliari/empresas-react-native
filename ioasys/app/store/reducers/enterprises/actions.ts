import {EnterpriseInfo, EnterprisesActionTypes} from './types';
import {
  GET_ENTERPRISES_FULFILLED,
  GET_ENTERPRISES_REJECTED,
  GET_ENTERPRISES_REQUEST,
} from './actionTypes';

export function getEnterprisesRequest(params: {
  enterpriseTypes: string;
  name: string;
}): EnterprisesActionTypes {
  return {
    type: GET_ENTERPRISES_REQUEST,
    payload: {
      params,
    },
  };
}

export function getEnterprisesFulfilled(
  enterprises: EnterpriseInfo[],
): EnterprisesActionTypes {
  return {
    type: GET_ENTERPRISES_FULFILLED,
    payload: {
      enterprises,
    },
  };
}

export function getEnterprisesRejected(error: string): EnterprisesActionTypes {
  return {
    type: GET_ENTERPRISES_REJECTED,
    payload: {
      error,
    },
  };
}
