import {
  GET_ENTERPRISES_FULFILLED,
  GET_ENTERPRISES_REJECTED,
  GET_ENTERPRISES_REQUEST,
} from './actionTypes';

export interface EnterpriseType {
  id: string;
  enterpriseTypeName: string;
}

export interface EnterpriseInfo {
  id: string;
  emailEnterprise: string;
  facebook: string;
  twitter: string;
  linkedIn: string;
  phone: string;
  ownEnterprise: string;
  enterpriseName: string;
  photo: string;
  description: string;
  city: string;
  country: string;
  value: string;
  sharePrice: string;
  enterpriseType: EnterpriseType;
}

export interface EnterprisesState {
  enterprises: EnterpriseInfo[] | [];
  error: string | null;
}

export interface GetEnterprisesFulfilledAction {
  type: typeof GET_ENTERPRISES_FULFILLED;
  payload: {
    enterprises: EnterpriseInfo[];
  };
}

export interface GetEnterprisesRejectedAction {
  type: typeof GET_ENTERPRISES_REJECTED;
  payload: {
    error: string;
  };
}

export interface GetEnterprisesRequestAction {
  type: typeof GET_ENTERPRISES_REQUEST;
  payload: {
    params: {
      enterpriseTypes: string;
      name: string;
    };
  };
}

export type EnterprisesActionTypes =
  | GetEnterprisesFulfilledAction
  | GetEnterprisesRejectedAction
  | GetEnterprisesRequestAction;
