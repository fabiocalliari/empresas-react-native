import {AppState} from '..';
import R from 'ramda';
import {createSelector} from 'reselect';

export const selectEnterprises = createSelector(
  (state: AppState) => state.enterprises,
  (enterprises) => enterprises.enterprises,
);

export const selectEnterpriseTypes = createSelector(
  (state: AppState) => selectEnterprises(state),
  (enterprises) =>
    R.uniqBy(
      R.prop('id'),
      R.map((v) => v.enterpriseType, enterprises),
    ),
);
