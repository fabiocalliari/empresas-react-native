import authReducer from './auth/reducers';
import {combineReducers} from 'redux';
import enterprisesReducer from './enterprises/reducers';
import searchReducer from './search/reducers';
import uiReducer from './ui/reducers';

const rootReducer = combineReducers({
  auth: authReducer,
  enterprises: enterprisesReducer,
  search: searchReducer,
  ui: uiReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
