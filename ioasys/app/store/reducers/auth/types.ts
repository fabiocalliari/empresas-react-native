import {
  SIGN_IN_FULFILLED,
  SIGN_IN_REJECTED,
  SIGN_IN_REQUEST,
} from './actionTypes';

export interface UserProfile {
  id: string;
  investorName: string;
  email: string;
  city: string;
  country: string;
  balance: string;
  photo: string;
  firstAccess: string;
  superAngel: string;
}

export interface AuthState {
  userProfile: UserProfile | null;
  error: string | null;
}

export interface SignInFulfilledAction {
  type: typeof SIGN_IN_FULFILLED;
  payload: {
    userProfile: UserProfile;
  };
}

export interface SignInRejectedAction {
  type: typeof SIGN_IN_REJECTED;
  payload: {
    error: string;
  };
}

export interface SignInRequestAction {
  type: typeof SIGN_IN_REQUEST;
  payload: {
    email: string;
    password: string;
  };
}

export type AuthActionTypes =
  | SignInFulfilledAction
  | SignInRejectedAction
  | SignInRequestAction;
