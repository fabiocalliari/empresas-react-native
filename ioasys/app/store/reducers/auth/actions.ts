import {AuthActionTypes, UserProfile} from './types';
import {
  SIGN_IN_FULFILLED,
  SIGN_IN_REJECTED,
  SIGN_IN_REQUEST,
} from './actionTypes';

export function signInRequest(
  email: string,
  password: string,
): AuthActionTypes {
  return {
    type: SIGN_IN_REQUEST,
    payload: {
      email,
      password,
    },
  };
}

export function signInFulfilled(userProfile: UserProfile): AuthActionTypes {
  return {
    type: SIGN_IN_FULFILLED,
    payload: {
      userProfile,
    },
  };
}

export function signInRejected(error: string): AuthActionTypes {
  return {
    type: SIGN_IN_REJECTED,
    payload: {
      error,
    },
  };
}
