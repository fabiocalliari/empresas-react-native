import {AuthActionTypes, AuthState} from './types';
import {
  SIGN_IN_FULFILLED,
  SIGN_IN_REJECTED,
  SIGN_IN_REQUEST,
} from './actionTypes';
import produce, {enableES5} from 'immer';

enableES5();

const initialState: AuthState = {
  userProfile: null,
  error: null,
};

export default (state = initialState, action: AuthActionTypes) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SIGN_IN_REQUEST: {
        draft.userProfile = null;
        draft.error = null;
        break;
      }
      case SIGN_IN_FULFILLED: {
        draft.userProfile = action.payload.userProfile;
        break;
      }
      case SIGN_IN_REJECTED: {
        draft.userProfile = null;
        draft.error = action.payload.error;
        break;
      }
      default:
        return draft;
    }
  });
