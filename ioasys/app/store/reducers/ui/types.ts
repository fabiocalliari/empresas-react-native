import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  START_ACTION,
  STOP_ACTION,
  UPDATE_NOTIFICATION,
} from './actionTypes';

import {Theme} from '_theme/index';

export interface Action {
  name: string;
  params: object;
}

export interface Notification {
  id?: number;
  message: string;
  open?: boolean;
  variant?: Exclude<keyof Theme['toastVariants'], 'defaults'>;
}

export interface UiState {
  loader: {
    actions: Action[];
  };
  notifications: Notification[];
}

export interface StartAction {
  type: typeof START_ACTION;
  payload: {
    action: Action;
  };
}

export interface StopAction {
  type: typeof STOP_ACTION;
  payload: {
    name: string;
  };
}

export interface AddNotificationAction {
  type: typeof ADD_NOTIFICATION;
  payload: {
    notification: Notification;
  };
}

export interface RemoveNotificationAction {
  type: typeof REMOVE_NOTIFICATION;
  payload: {
    id: Notification['id'];
  };
}

export interface UpdateNotificationAction {
  type: typeof UPDATE_NOTIFICATION;
  payload: {
    notification: Notification;
  };
}

export type UiActionTypes =
  | StartAction
  | StopAction
  | AddNotificationAction
  | RemoveNotificationAction
  | UpdateNotificationAction;
