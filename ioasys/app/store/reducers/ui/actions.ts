import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  START_ACTION,
  STOP_ACTION,
  UPDATE_NOTIFICATION,
} from './actionTypes';

import {Notification} from './types';

export const startAction = (name: string, params: object) => ({
  type: START_ACTION,
  payload: {
    action: {
      name,
      params,
    },
  },
});

export const stopAction = (name: string) => ({
  type: STOP_ACTION,
  payload: {name},
});

export const addNotification = (notification: Notification) => ({
  type: ADD_NOTIFICATION,
  payload: {notification},
});

export const removeNotification = (id: Notification['id']) => ({
  type: REMOVE_NOTIFICATION,
  payload: {id},
});

export const updateNotification = (notification: Notification) => ({
  type: UPDATE_NOTIFICATION,
  payload: {notification},
});
