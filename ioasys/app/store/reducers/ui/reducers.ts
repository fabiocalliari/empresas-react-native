import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  START_ACTION,
  STOP_ACTION,
  UPDATE_NOTIFICATION,
} from './actionTypes';
import {UiActionTypes, UiState} from './types';
import produce, {enableES5} from 'immer';

enableES5();

const initialState: UiState = {
  loader: {
    actions: [],
  },
  notifications: [],
};

let id = 0;

export default (state = initialState, action: UiActionTypes) =>
  produce(state, (draft) => {
    switch (action.type) {
      case START_ACTION: {
        draft.loader.actions.push(action.payload.action);
        break;
      }
      case STOP_ACTION: {
        draft.loader.actions.splice(
          draft.loader.actions.findIndex((v) => v.name === action.payload.name),
          1,
        );
        break;
      }
      case ADD_NOTIFICATION: {
        draft.notifications.push({
          id: ++id,
          ...action.payload.notification,
          open: true,
        });
        break;
      }
      case REMOVE_NOTIFICATION: {
        draft.notifications.splice(
          draft.notifications.findIndex((v) => v.id === action.payload.id),
          1,
        );
        break;
      }
      case UPDATE_NOTIFICATION: {
        Object.assign(
          draft.notifications.find(
            (v) => v.id === action.payload.notification.id,
          ),
          {...action.payload.notification},
        );
        break;
      }
      default:
        return draft;
    }
  });
