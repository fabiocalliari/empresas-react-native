import {AppState} from '..';

export const checkIfLoading = (store: AppState, ...actionsToCheck) =>
  store.ui.loader.actions.some((action) =>
    actionsToCheck.includes(action.name),
  );
