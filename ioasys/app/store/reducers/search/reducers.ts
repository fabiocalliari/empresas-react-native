import {SearchActionTypes, SearchState} from './types';
import produce, {enableES5} from 'immer';

import {SET_SEARCH_CONTEXT} from './actionTypes';

enableES5();

const initialState: SearchState = {
  searchContext: 'static',
};

export default (state = initialState, action: SearchActionTypes) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_SEARCH_CONTEXT: {
        draft.searchContext = action.payload.searchContext;
        break;
      }
      default:
        return draft;
    }
  });
