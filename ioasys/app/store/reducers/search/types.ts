import {SET_SEARCH_CONTEXT} from './actionTypes';

export type SearchContext = 'static' | 'searching';

export interface SearchState {
  searchContext: SearchContext;
}

export interface SetSearchContextAction {
  type: typeof SET_SEARCH_CONTEXT;
  payload: {
    searchContext: SearchContext;
  };
}

export type SearchActionTypes = SetSearchContextAction;
