import {SET_SEARCH_CONTEXT} from './actionTypes';
import {SearchActionTypes} from './types';

export function setSearchContext(
  searchContext: 'static' | 'searching',
): SearchActionTypes {
  return {
    type: SET_SEARCH_CONTEXT,
    payload: {
      searchContext,
    },
  };
}
