import {applyMiddleware, compose, createStore} from 'redux';
import rootReducer, {AppState} from './reducers';

import {Api} from '_services/api';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import tron from '../utils/reactotron-config';

export default function configureStore({
  preloadedState,
  api,
}: {
  preloadedState: AppState;
  api: Api;
}) {
  const middlewares = [];

  /* Saga */
  const sagaMonitor = __DEV__ ? tron.createSagaMonitor() : null;
  const sagaMiddleware = createSagaMiddleware({context: {api}, sagaMonitor});
  middlewares.push(sagaMiddleware);

  let enhancer = applyMiddleware(...middlewares);

  if (__DEV__) {
    enhancer = compose(applyMiddleware(...middlewares), tron.createEnhancer());
  }

  /* Store */
  const store = createStore(rootReducer, preloadedState, enhancer);

  /* Run Saga */
  let sagaTask = sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers').default;
      store.replaceReducer(nextReducer);
    });

    module.hot.accept('./sagas', () => {
      const nextSagas = require('./sagas').default;
      sagaTask.cancel();
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(nextSagas);
      });
    });
  }

  return store;
}
