import {Theme} from '_theme/index';
import {createBox} from '@shopify/restyle';

const Box = createBox<Theme>();

export default Box;
