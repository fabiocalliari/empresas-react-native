import {Box, Text} from '..';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import chroma from 'chroma-js';
import {defaultColors} from './constants';
import {generateBackgroundColor} from '_utils/generate-background-color';
import {makeStyles} from '_theme/make-styles';

interface ChipProps {
  text: string;
  isSelected?: boolean;
}

const useStyles = makeStyles((theme, props) => {
  let color;
  const backgroundColor = generateBackgroundColor(props.text, defaultColors);
  const colorObject = chroma(backgroundColor.toString());

  if (colorObject.get('lab.l') < 70) {
    color = colorObject.brighten(2);
  } else {
    color = colorObject.darken(2);
  }

  return {
    chipView: {
      alignItems: 'center',
      backgroundColor,
      borderRadius: 99,
      paddingHorizontal: theme.spacing.m,
      paddingVertical: theme.spacing.xxs,
    },
    chipText: {
      color: color.hex(),
    },
    chipIcon: {
      color: color.hex(),
      fontSize: 14,
      marginLeft: theme.spacing.xxs,
    },
  };
});

const Chip: React.FC<ChipProps> = ({isSelected, text}) => {
  const styles = useStyles({text});

  return (
    <Box flexDirection="row" style={styles.chipView}>
      <Text variant="caption" style={styles.chipText}>
        {text}
      </Text>
      {isSelected ? <Icon name="close" style={styles.chipIcon} /> : null}
    </Box>
  );
};

export default Chip;
