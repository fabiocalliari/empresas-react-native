import {SafeAreaView, SafeAreaViewProps} from 'react-native-safe-area-context';

import {Theme} from '_theme/index';
import {createBox} from '@shopify/restyle';

const SafeAreaBox = createBox<Theme, SafeAreaViewProps>(SafeAreaView);

export default SafeAreaBox;
