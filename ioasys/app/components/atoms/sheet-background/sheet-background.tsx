import {BottomSheetBackgroundProps} from '@gorhom/bottom-sheet';
import {Box} from '_atoms/box';
import React from 'react';

interface SheetBackgroundProps extends BottomSheetBackgroundProps {}

const SheetBackground: React.FC<SheetBackgroundProps> = (props) => {
  return (
    <Box
      {...props}
      backgroundColor="card"
      borderTopLeftRadius="xl"
      borderTopRightRadius="xl"
    />
  );
};

export default SheetBackground;
