import {Box} from '..';
import React from 'react';

const Divider = () => {
  return (
    <Box backgroundColor="border" flex={1} height={1} marginVertical="l" />
  );
};

export default Divider;
