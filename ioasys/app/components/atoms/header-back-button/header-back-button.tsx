import PressBox, {PressBoxProps} from '_atoms/press-box/press-box';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {IconProps} from 'react-native-vector-icons/Icon';
import React from 'react';
import {Theme} from '_theme/index';
import chroma from 'chroma-js';
import {useTheme} from '@shopify/restyle';

type HeaderBackButtonProps = Omit<IconProps, 'name'> & PressBoxProps;

const HeaderBackButton = ({
  onDoubleTap,
  onLongPress,
  onTap,
  ...props
}: HeaderBackButtonProps) => {
  const {colors, spacing} = useTheme<Theme>();
  return (
    <PressBox onDoubleTap={onDoubleTap} onLongPress={onLongPress} onTap={onTap}>
      <Icon
        color={colors.text}
        {...props}
        name="arrow-left"
        style={[
          props.style,
          {
            backgroundColor: chroma(colors.background).alpha(0.5).hex(),
            borderRadius: 20,
            marginHorizontal: spacing.m,
          },
        ]}
      />
    </PressBox>
  );
};

HeaderBackButton.defaultProps = {
  size: 24,
};

export default HeaderBackButton;
