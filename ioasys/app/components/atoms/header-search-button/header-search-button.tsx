import PressBox, {PressBoxProps} from '_atoms/press-box/press-box';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {IconProps} from 'react-native-vector-icons/Icon';
import React from 'react';
import {Theme} from '_theme/index';
import chroma from 'chroma-js';
import {useTheme} from '@shopify/restyle';

type HeaderSearchButtonProps = Omit<IconProps, 'name'> & PressBoxProps;

const HeaderSearchButton = ({
  onDoubleTap,
  onLongPress,
  onTap,
  ...props
}: HeaderSearchButtonProps) => {
  const {colors, spacing} = useTheme<Theme>();
  return (
    <PressBox onDoubleTap={onDoubleTap} onLongPress={onLongPress} onTap={onTap}>
      <Icon
        color={colors.secondary}
        {...props}
        name="magnify"
        style={[
          props.style,
          {
            backgroundColor: chroma(colors.background).alpha(0.5).hex(),
            borderRadius: 20,
            marginHorizontal: spacing.m,
          },
        ]}
      />
    </PressBox>
  );
};

HeaderSearchButton.defaultProps = {
  size: 24,
};

export default HeaderSearchButton;
