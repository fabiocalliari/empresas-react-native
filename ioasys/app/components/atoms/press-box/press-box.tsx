import {
  LongPressGestureHandler,
  LongPressGestureHandlerStateChangeEvent,
  State,
  TapGestureHandler,
  TapGestureHandlerStateChangeEvent,
} from 'react-native-gesture-handler';
import React, {useRef} from 'react';

export type PressBoxProps = {
  onTap?: (event: TapGestureHandlerStateChangeEvent) => void;
  onLongPress?: (event: LongPressGestureHandlerStateChangeEvent) => void;
  onDoubleTap?: (event: TapGestureHandlerStateChangeEvent) => void;
};

const PressBox: React.FC<PressBoxProps> = ({
  children,
  onDoubleTap,
  onLongPress,
  onTap,
}) => {
  const doubleTapRef = useRef<TapGestureHandler>(null);

  const _onHandlerStateChange = (
    event: LongPressGestureHandlerStateChangeEvent,
  ) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      onLongPress?.(event);
    }
  };

  const _onSingleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      onTap?.(event);
    }
  };
  const _onDoubleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      onDoubleTap?.(event);
    }
  };

  return (
    <LongPressGestureHandler
      onHandlerStateChange={_onHandlerStateChange}
      minDurationMs={800}>
      <TapGestureHandler
        onHandlerStateChange={_onSingleTap}
        waitFor={doubleTapRef}>
        <TapGestureHandler
          ref={doubleTapRef}
          onHandlerStateChange={_onDoubleTap}
          numberOfTaps={2}>
          {children}
        </TapGestureHandler>
      </TapGestureHandler>
    </LongPressGestureHandler>
  );
};

export default PressBox;
