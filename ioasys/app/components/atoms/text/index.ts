import {default as Text} from './text';
import {textVariants} from './text-variants';

export {Text, textVariants};
