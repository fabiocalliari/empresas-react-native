import {Theme} from '_theme/index';
import {createText} from '@shopify/restyle';

const Text = createText<Theme>();

export default Text;
