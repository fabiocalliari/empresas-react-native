export const textVariants = {
  defaults: {
    color: 'text',
  },
  header: {
    fontSize: 34,
    fontWeight: 'normal',
    letterSpacing: 0.25,
  },
  subheader: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 0.15,
  },
  body: {
    fontSize: 16,
    fontWeight: 'normal',
    letterSpacing: 0.5,
  },
  subtitle: {
    fontSize: 14,
    fontWeight: 'bold',
    letterSpacing: 0.1,
  },
  caption: {
    fontSize: 12,
    fontWeight: 'normal',
    letterSpacing: 0.4,
  },
  overline: {
    fontSize: 10,
    fontWeight: 'bold',
    letterSpacing: 1.5,
    textTransform: 'uppercase',
  },
  button: {
    fontSize: 14,
    fontWeight: 'bold',
    letterSpacing: 1.25,
  },
};
