import React, {forwardRef, useState} from 'react';
import TextInput, {TextInputProps} from '../text-input/text-input';
import {
  UseControllerOptions,
  useController,
  useFormContext,
} from 'react-hook-form';

import {TextInput as RNTextInput} from 'react-native';

const ControlledPasswordInput = forwardRef<
  RNTextInput,
  Omit<TextInputProps, 'secureTextEntry' | 'trailing'> & UseControllerOptions
>((props, forwardedRef) => {
  const {name, rules, defaultValue = '', ...inputProps} = props;

  const formContext = useFormContext();
  const {control, errors} = formContext;

  const {field} = useController({name, control, rules, defaultValue});

  const [passwordVisible, setPasswordVisible] = useState(false);

  const renderTrailing = () => {
    if (passwordVisible) {
      return (
        <TextInput.Icon name="eye" onTap={() => setPasswordVisible(false)} />
      );
    }

    return (
      <TextInput.Icon name="eye-off" onTap={() => setPasswordVisible(true)} />
    );
  };

  return (
    <TextInput
      {...inputProps}
      error={!!errors[name]?.message}
      helperText={errors[name]?.message}
      onBlur={field.onBlur}
      onChangeText={field.onChange}
      value={field.value}
      ref={forwardedRef}
      secureTextEntry={!passwordVisible}
      trailing={renderTrailing}
    />
  );
});

export default ControlledPasswordInput;
