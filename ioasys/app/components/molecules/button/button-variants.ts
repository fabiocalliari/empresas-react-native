export const buttonVariants = {
  primary: {
    backgroundColor: 'primary',
    color: 'onPrimary',
  },
  primaryOutline: {
    borderColor: 'primary',
    color: 'primary',
  },
  primaryText: {
    color: 'primary',
  },
};
