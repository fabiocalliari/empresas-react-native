import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {
  BackgroundColorProps,
  BorderProps,
  ColorProps,
  LayoutProps,
  SpacingProps,
  VariantProps,
  backgroundColor,
  border,
  color,
  createVariant,
  layout,
  spacing,
  useRestyle,
} from '@shopify/restyle';
import {PressBox, Text} from '_atoms/index';

import {ButtonIcon} from './icon';
import {PressBoxProps} from '_atoms/press-box/press-box';
import R from 'ramda';
import React from 'react';
import {Theme} from '_theme/index';

export type ButtonProps = BackgroundColorProps<Theme> &
  BorderProps<Theme> &
  ColorProps<Theme> &
  LayoutProps<Theme> &
  SpacingProps<Theme> &
  VariantProps<Theme, 'buttonVariants'> &
  PressBoxProps & {
    icon?: any;
    isLoading?: boolean;
  };

const restyleFunctions = [
  backgroundColor,
  border,
  color,
  layout,
  spacing,
  createVariant<Theme>({themeKey: 'buttonVariants'}),
];

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderRadius: 99,
    borderWidth: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  label: {
    paddingHorizontal: 4,
  },
});

const Button: React.FC<ButtonProps> & {
  Icon: typeof ButtonIcon;
} = ({children, icon, isLoading, onDoubleTap, onLongPress, onTap, ...rest}) => {
  const props = useRestyle(restyleFunctions, rest);
  //@ts-ignore
  const {style} = props;

  const buttonStyle = R.mergeAll(R.flatten([styles.container, style]));

  const labelStyle = R.mergeAll(
    R.flatten([styles.label, {color: buttonStyle.color}]),
  );

  const renderIcon = () => {
    if (typeof icon !== 'undefined') {
      return icon;
    }

    return null;
  };

  const renderLoading = () => {
    if (!isLoading) {
      return null;
    }
    return <ActivityIndicator color={buttonStyle.color} size={16} />;
  };

  return (
    <PressBox onDoubleTap={onDoubleTap} onLongPress={onLongPress} onTap={onTap}>
      <View style={buttonStyle}>
        {renderIcon()}
        <Text
          numberOfLines={1}
          style={labelStyle}
          textAlign="center"
          variant="button">
          {children}
        </Text>
        {renderLoading()}
      </View>
    </PressBox>
  );
};

Button.Icon = ButtonIcon;

export default Button;
