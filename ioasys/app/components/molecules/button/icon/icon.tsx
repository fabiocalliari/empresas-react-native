import {IconProps} from 'react-native-vector-icons/Icon';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import {StyleSheet} from 'react-native';
import {makeStyles} from '_theme/make-styles';

const {flatten} = StyleSheet;

const useStyles = makeStyles((theme) => ({
  icon: {
    paddingHorizontal: theme.spacing.s,
  },
}));

const ButtonIcon = (props: IconProps) => {
  const styles = useStyles();

  const iconStyle = flatten([styles.icon, props.style]);

  return <MaterialIcons {...props} style={iconStyle} />;
};

ButtonIcon.defaultProps = {
  size: 16,
};

export default ButtonIcon;
