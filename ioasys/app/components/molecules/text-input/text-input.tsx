import {Easing, set, useCode, useValue} from 'react-native-reanimated';
import {
  LayoutChangeEvent,
  NativeSyntheticEvent,
  TextInput as RNTextInput,
  TextInputProps as RNTextInputProps,
  StyleProp,
  TextInputFocusEventData,
  TextInputSubmitEditingEventData,
  View,
  ViewStyle,
} from 'react-native';
import React, {RefObject, forwardRef, useCallback, useState} from 'react';

import {AccessoryView} from './accessory-view';
import {AccessoryViewProps} from './accessory-view/accessory-view';
import {FloatingLabel} from './floating-label';
import {InputIcon} from './icon';
import {Text} from '_atoms/index';
import {Underline} from './underline';
import chroma from 'chroma-js';
import {makeStyles} from '_theme/make-styles';
import {timing} from 'react-native-redash/lib/module/v1';

export type TextInputProps = Omit<
  RNTextInputProps,
  'clearButtonMode' | 'underlineColorAndroid'
> & {
  /**
   * The style that should be applied to the outermost container.
   */
  containerStyle?: StyleProp<ViewStyle>;
  /**
   * Whether the input is valid or not.
   */
  error?: boolean;
  /**
   * The helper that is shown under the input
   *
   * - When error is true, the text component receives a contextual theme.
   */
  helperText?: string;
  /**
   * The floating label.
   */
  label: string;
  /**
   * Helper to focus next input on submit editing.
   */
  nextInput?: RefObject<RNTextInput>;
  /**
   * An element to be appended to the input, e.g. a icon or a button.
   */
  trailing?: AccessoryViewProps<any>;
};

const useStyles = makeStyles((theme) => ({
  container: {
    alignItems: 'stretch',
  },
  stack: {
    alignItems: 'center',
    backgroundColor: chroma(theme.colors.text).alpha(0.1).toString(),
    borderColor: theme.colors.secondary,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    flexDirection: 'row',
  },
  textInput: {
    color: theme.colors.text,
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    fontWeight: 'normal',
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 'auto',
    paddingBottom: theme.spacing.xxs,
    paddingHorizontal: theme.spacing.s,
    paddingTop: theme.spacing.l,
  },
}));

const TextInput: React.FC<TextInputProps> = forwardRef<
  RNTextInput,
  TextInputProps
>((props, forwardedRef) => {
  const {
    containerStyle,
    error,
    helperText,
    label,
    nextInput,
    onBlur,
    onChange,
    onChangeText,
    onFocus,
    onSubmitEditing,
    placeholder,
    trailing,
    value,
    ...rest
  } = props;
  //#region local state
  const [focused, setFocused] = useState<boolean>(false);
  const [containerLayout, setContainerLayout] = useState<{
    height: number;
    width: number;
  }>({
    height: 0,
    width: 0,
  });
  //#endregion

  //#region local variables
  const styles = useStyles();

  const animationState = useValue(value !== '' ? 1 : 0);
  //#endregion

  //#region local functions
  const onContainerLayoutChange = useCallback(
    ({nativeEvent}: LayoutChangeEvent) => {
      setContainerLayout({
        height: nativeEvent.layout.height,
        width: nativeEvent.layout.width,
      });
    },
    [],
  );

  const handleOnBlur = useCallback(
    (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
      setFocused(false);
      onBlur?.(e);
    },
    [onBlur],
  );
  const handleOnFocus = useCallback(
    (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
      setFocused(true);
      onFocus?.(e);
    },
    [onFocus],
  );
  const handleOnSubmitEditing = useCallback(
    (e: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => {
      const focusNextInput = () => {
        if (nextInput?.current) {
          nextInput.current.focus();
        }
      };
      focusNextInput();
      onSubmitEditing?.(e);
    },
    [nextInput, onSubmitEditing],
  );
  //#endregion

  //#region effects
  useCode(
    () =>
      set(
        animationState,
        timing({
          duration: 150,
          from: animationState,
          to: focused || value !== '' ? 1 : 0,
          easing: Easing.linear,
        }),
      ),
    [focused, value],
  );
  //#endregion

  return (
    <View style={[styles.container, containerStyle]}>
      <View>
        <View onLayout={onContainerLayoutChange} style={styles.stack}>
          <RNTextInput
            {...rest}
            clearButtonMode="never"
            onBlur={handleOnBlur}
            onChange={onChange}
            onChangeText={onChangeText}
            onFocus={handleOnFocus}
            onSubmitEditing={handleOnSubmitEditing}
            placeholder={focused ? placeholder : ''}
            placeholderTextColor={styles.textInput.color}
            ref={forwardedRef}
            style={styles.textInput}
            underlineColorAndroid="transparent"
            value={value}
          />
          <AccessoryView component={trailing} />
        </View>

        <FloatingLabel
          animationState={animationState}
          containerLayout={containerLayout}
          error={error}>
          {label}
        </FloatingLabel>

        <Underline animationState={animationState} error={error} />
      </View>
      <Text
        color={error ? 'notification' : 'text'}
        numberOfLines={1}
        paddingHorizontal="s">
        {helperText}
      </Text>
    </View>
  );
});

TextInput.Icon = InputIcon;

export default TextInput;
