import Animated, {
  Easing,
  Extrapolate,
  interpolate,
  interpolateColors,
  set,
  useCode,
  useValue,
} from 'react-native-reanimated';
import {LayoutChangeEvent, StyleProp, ViewStyle} from 'react-native';
import React, {useCallback, useState} from 'react';

import {Theme} from '_theme/index';
import {makeStyles} from '_theme/make-styles';
import {timing} from 'react-native-redash/lib/module/v1';
import {useTheme} from '@shopify/restyle';

interface FloatingLabelProps {
  animationState: Animated.Value<number>;
  containerLayout: {height: number; width: number};
  error?: boolean;
}

const useStyles = makeStyles((theme) => ({
  labelWrapper: {
    position: 'absolute',
    left: theme.spacing.s,
  },
  label: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    fontWeight: 'normal',
  },
}));

const FloatingLabel: React.FC<FloatingLabelProps> = ({
  animationState,
  children,
  containerLayout,
  error,
}) => {
  // since there's a lag until the measurements are done,
  // let's make a nice animation to obfuscate it
  const animatedMeasure = useValue(0);

  const styles = useStyles();

  const {colors} = useTheme<Theme>();
  const [labelWrapperLayout, setLabelWrapperLayout] = useState<{
    height: number;
    width: number;
  }>({
    height: 0,
    width: 0,
  });

  let labelStyle: StyleProp<ViewStyle>;

  const animatedLabelWrapperStyle = {
    transform: [
      {
        translateY: interpolate(animationState, {
          inputRange: [0, 1],
          outputRange: [
            labelWrapperLayout.height > 0
              ? (containerLayout.height - labelWrapperLayout.height) / 2
              : 0,
            0,
          ],
          extrapolate: Extrapolate.CLAMP,
        }),
      },
      {
        translateX: -labelWrapperLayout.width / 2,
      },
      {
        scale: interpolate(animationState, {
          inputRange: [0, 1],
          outputRange: [1, 0.7],
          extrapolate: Extrapolate.CLAMP,
        }),
      },
      {
        translateX: labelWrapperLayout.width / 2,
      },
    ],
  };

  const color = interpolateColors(animationState, {
    inputRange: [0, 1],
    outputColorRange: [colors.text, colors.primary],
  });
  const animatedLabelStyle = {
    color: error ? colors.notification : color,
    opacity: interpolate(animatedMeasure, {
      inputRange: [0, 1],
      outputRange: [0, 1],
    }),
    transform: [
      {
        translateY: interpolate(animatedMeasure, {
          inputRange: [0, 1],
          outputRange: [4, 0],
        }),
      },
    ],
  };

  //#region local functions
  const onLabelWrapperLayoutChange = useCallback(
    ({nativeEvent}: LayoutChangeEvent) => {
      setLabelWrapperLayout({
        height: nativeEvent.layout.height,
        width: nativeEvent.layout.width,
      });
    },
    [],
  );
  //#endregion

  //#region effects
  useCode(
    () =>
      set(
        animatedMeasure,
        timing({
          duration: 100,
          from: animatedMeasure,
          to: [containerLayout, labelWrapperLayout].some((v) => v.height === 0)
            ? 0
            : 1,
          easing: Easing.linear,
        }),
      ),
    [containerLayout, labelWrapperLayout],
  );
  //#endregion

  return (
    <Animated.View
      onLayout={onLabelWrapperLayoutChange}
      pointerEvents="none"
      style={[styles.labelWrapper, animatedLabelWrapperStyle]}>
      <Animated.Text
        // since animated color is a number, we must cast it to any to avoid typing warning
        style={[styles.label, labelStyle, animatedLabelStyle as any]}>
        {children}
      </Animated.Text>
    </Animated.View>
  );
};

export default FloatingLabel;
