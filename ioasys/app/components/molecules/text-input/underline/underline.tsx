import Animated, {interpolate} from 'react-native-reanimated';

import React from 'react';
import {StyleSheet} from 'react-native';
import {Theme} from '_theme/index';
import {useTheme} from '@shopify/restyle';

interface UnderlineProps {
  animationState: Animated.Value<number>;
  error?: boolean;
}

const styles = StyleSheet.create({
  underline: {
    bottom: 0,
    position: 'absolute',
    left: 0,
    right: 0,
  },
});

const Underline: React.FC<UnderlineProps> = ({animationState, error}) => {
  const {colors} = useTheme<Theme>();

  const animatedUnderlineStyle = {
    backgroundColor: error ? colors.notification : colors.primary,
    height: interpolate(animationState, {
      inputRange: [0, 1],
      outputRange: [1, 2],
    }),
    transform: [
      {
        scaleX: interpolate(animationState, {
          inputRange: [0, 1],
          outputRange: [0, 1],
        }),
      },
    ],
  };
  return <Animated.View style={[styles.underline, animatedUnderlineStyle]} />;
};

export default Underline;
