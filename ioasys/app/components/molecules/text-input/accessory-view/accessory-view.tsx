import React from 'react';

export type RenderProp<Props = {}> = (props?: Props) => React.ReactElement;

export type AccessoryViewProps<Props = {}> = Props & {
  component?: RenderProp<Props>;
};

function AccessoryView<Props = {}>({
  component: Component,
  ...props
}: AccessoryViewProps<Props>): React.ReactElement {
  if (!Component) {
    return null;
  }

  return <Component {...(props as Props)} />;
}

export default AccessoryView;
