import PressBox, {PressBoxProps} from '_atoms/press-box/press-box';
import {StyleSheet, View} from 'react-native';

import {IconProps} from 'react-native-vector-icons/Icon';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import {makeStyles} from '_theme/make-styles';

const {flatten} = StyleSheet;

const useStyles = makeStyles((theme) => ({
  trailing: {
    paddingHorizontal: theme.spacing.s,
  },
  trailingIcon: {
    color: theme.colors.secondary,
  },
}));

const InputIcon = ({
  onDoubleTap,
  onLongPress,
  onTap,
  ...props
}: IconProps & PressBoxProps) => {
  const styles = useStyles();

  const iconStyle = flatten([styles.trailingIcon, props.style]);

  return (
    <PressBox onDoubleTap={onDoubleTap} onTap={onTap} onLongPress={onLongPress}>
      <View style={styles.trailing}>
        <MaterialIcons {...props} style={iconStyle} />
      </View>
    </PressBox>
  );
};

InputIcon.defaultProps = {
  size: 22,
};

export default InputIcon;
