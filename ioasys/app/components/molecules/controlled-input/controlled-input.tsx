import {TextInput as RNTextInput, View} from 'react-native';
import React, {forwardRef} from 'react';
import TextInput, {TextInputProps} from '../text-input/text-input';
import {
  UseControllerOptions,
  useController,
  useFormContext,
} from 'react-hook-form';

const ControlledInput = forwardRef<
  RNTextInput,
  TextInputProps & UseControllerOptions
>((props, forwardedRef) => {
  const {name, rules, defaultValue = '', ...inputProps} = props;

  const formContext = useFormContext();
  const {control, errors} = formContext;

  const {field} = useController({name, control, rules, defaultValue});

  const renderTrailing = () => {
    return field.value ? (
      <TextInput.Icon
        name="close"
        onTap={() => formContext.setValue(name, '')}
      />
    ) : null;
  };

  return (
    <TextInput
      {...inputProps}
      error={!!errors[name]?.message}
      helperText={errors[name]?.message}
      onBlur={field.onBlur}
      onChangeText={field.onChange}
      value={field.value}
      ref={forwardedRef}
      trailing={renderTrailing}
    />
  );
});

export default ControlledInput;
