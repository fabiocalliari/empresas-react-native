export const toastVariants = {
  defaults: {
    backgroundColor: 'background',
    color: 'text',
  },
  error: {
    backgroundColor: 'notification',
    color: 'onNotification',
  },
};
