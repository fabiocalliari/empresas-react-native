import Animated, {
  call,
  clockRunning,
  cond,
  interpolate,
  not,
  set,
  useCode,
  useValue,
} from 'react-native-reanimated';
import {
  BackgroundColorProps,
  ColorProps,
  VariantProps,
  backgroundColor,
  color,
  createVariant,
  useRestyle,
} from '@shopify/restyle';
import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import {spring, useClock} from 'react-native-redash/lib/module/v1';

import {EdgeInsets} from 'react-native-safe-area-context';
import R from 'ramda';
import {Text} from '_components/atoms';
import {Theme} from '_theme/index';
import {View} from 'react-native';
import {makeStyles} from '_theme/make-styles';

type ToastProps = BackgroundColorProps<Theme> &
  ColorProps<Theme> &
  VariantProps<Theme, 'toastVariants'> & {
    autoHideDuration?: number | null;
    edgeInsets: EdgeInsets;
    message: string;
    onAnimationFinish: () => void;
    onClose: (...args: any[]) => void;
    open: boolean;
  };

const restyleFunctions = [
  backgroundColor,
  color,
  createVariant<Theme>({themeKey: 'toastVariants'}),
];

const useStyles = makeStyles((theme, {edgeInsets}) => ({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    left: edgeInsets.left,
    position: 'absolute',
    right: edgeInsets.right,
    top: edgeInsets.top,
  },
  contentContainer: {
    alignItems: 'center',
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadii.s,
    flexDirection: 'row',
    flexGrow: 1,
    flexWrap: 'wrap',
    paddingHorizontal: theme.spacing.s,
    paddingVertical: theme.spacing.m,
    width: '100%',
  },
  text: {
    color: theme.colors.text,
  },
  action: {
    alignItems: 'center',
    marginLeft: 'auto',
    paddingLeft: 16,
    marginRight: -8,
  },
}));

const Toast: FunctionComponent<ToastProps> = ({
  autoHideDuration,
  edgeInsets,
  message,
  open,
  onAnimationFinish,
  onClose,
  ...rest
}) => {
  //#region local variables

  const styles = useStyles({edgeInsets});

  const timerAutoHide = useRef<number>();

  //@ts-ignore
  const props = useRestyle(restyleFunctions, rest);
  const {style} = props as any;

  const toastStyle = R.mergeAll(R.flatten([styles.contentContainer, style]));

  const color = useMemo(() => toastStyle.color, [toastStyle]);
  const textStyle = R.mergeAll(R.flatten([styles.text, {color}]));

  //#endregion

  //#region animated properties

  const clock = useClock();

  const animatedVisibility = useValue(0);

  const animatedStyle = {
    opacity: interpolate(animatedVisibility, {
      inputRange: [0, 1],
      outputRange: [0, 1],
    }),
    transform: [
      {
        translateY: interpolate(animatedVisibility, {
          inputRange: [0, 1],
          outputRange: [200, 0],
        }),
      },
    ],
  };

  //#endregion

  //#region local functions

  const handleClose = useCallback(
    (...args: any[]) => {
      onClose(...args);
    },
    [onClose],
  );

  const setAutoHideTimer = useCallback(
    (autoHideDurationParam) => {
      if (!onClose || autoHideDurationParam == null) {
        return;
      }

      clearTimeout(timerAutoHide.current);
      timerAutoHide.current = setTimeout(() => {
        handleClose(null, 'timeout');
      }, autoHideDurationParam);
    },
    [handleClose, onClose],
  );

  //#endregion

  //#region effects

  useCode(
    () => [
      set(
        animatedVisibility,
        spring({clock, from: animatedVisibility, to: open ? 1 : 0}),
      ),
      cond(not(clockRunning(clock)), !open && call([], onAnimationFinish)),
    ],
    [open],
  );

  useEffect(() => {
    if (open) {
      setAutoHideTimer(autoHideDuration);
    }

    return () => {
      clearTimeout(timerAutoHide.current);
    };
  }, [open, autoHideDuration, setAutoHideTimer]);

  //#endregion

  return (
    <Animated.View style={[styles.container, animatedStyle]}>
      <View style={toastStyle}>
        <Text style={textStyle} variant="body">
          {message}
        </Text>
      </View>
    </Animated.View>
  );
};

export default Toast;
