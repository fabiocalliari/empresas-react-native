const presets = ['module:metro-react-native-babel-preset'];

const plugins = [
  [
    require.resolve('babel-plugin-module-resolver'),
    {
      root: ['./'],
      alias: {
        _assets: './assets',
        _atoms: './app/components/atoms',
        _components: './app/components',
        _core: './app/core',
        _hooks: './app/hooks',
        _molecules: './app/components/molecules',
        _navigation: './app/navigation',
        _organisms: './app/components/organisms',
        _reducers: './app/store/reducers',
        _sagas: './app/store/sagas',
        _screens: './app/screens',
        _services: './app/services',
        _store: './app/store',
        _theme: './app/theme',
        _utils: './app/utils',
      },
    },
  ],
];

module.exports = {
  presets,
  plugins,
};
